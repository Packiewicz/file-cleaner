Simple program to delete files in given directory.

Flags:
- `-y` Dont ask for confirmation
- `-path=[dir]` Path to directory where files should be deleted. If not specified, program uses cwd
- `-v` Verbose mode (displays path of every deleted file)
- `-r` Recursively delete files in all subfolders of the path