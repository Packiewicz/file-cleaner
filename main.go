package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
)

func removeFrom(path string) {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		log.Print(err)
		return
	}
	for _, f := range files {
		if !f.IsDir() {
			filePath := filepath.Join(path, f.Name())
			err = os.Remove(filePath)
			if err != nil {
				log.Print(err)
			}
		}
	}
}

// RemoveFiles is ...
func RemoveFiles(path string, recursive bool) {
	if !recursive {
		removeFrom(path)
	} else {
		filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				log.Printf("Problem with dir: %q\nError: %v", path, err)
				return err
			}
			if info.IsDir() {
				removeFrom(path)
			}
			return nil
		})
	}
}

func getPath(path *string) (string, error) {
	if *path == "" {
		cwd, err := os.Getwd()
		if err != nil {
			return "", err
		}
		return cwd, nil
	}
	return *path, nil
}

func main() {
	accept := flag.Bool("y", false, "Dont ask to confirm")
	verbose := flag.Bool("v", false, "Verbose")
	recursive := flag.Bool("r", false, "Delete files recursively")
	path := flag.String("path", "", "Path where to remove files")

	flag.Parse()

	if !*verbose {
		log.SetOutput(ioutil.Discard)
	}

	if *accept == false && *path == "" {
		fmt.Println("By default I remove everyhting in current directory, are you sure? [Y/N]")
		var a string
		fmt.Scanf("%s", &a)

		a = strings.ToLower(a)
		if a == "y" {
			*accept = true
		} else if a == "n" {
			fmt.Println("Nothing removed")
			os.Exit(0)
		} else {
			fmt.Println("Wrong input")
			os.Exit(1)
		}
	}

	actualPath, err := getPath(path)
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}

	RemoveFiles(actualPath, *recursive)
}
