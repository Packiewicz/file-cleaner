package main

import (
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"testing"
)

func countFiles(path string) int {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		log.Print(err)
		return 999
	}

	counter := 0
	for _, f := range files {
		if !f.IsDir() {
			counter++
		}
	}
	return counter
}

func testNonrecursiveRemoveAllFiles(t *testing.T) {
	// given
	cwd, err := os.Getwd()
	testDirPath := filepath.Join(cwd, "test-dir")
	innerDir := filepath.Join(testDirPath, "inner")

	err = os.Mkdir(testDirPath, 0755)
	err = os.Mkdir(innerDir, 0755)
	if err != nil {
		log.Fatal(err)
	}

	// create 3 text files
	for i := 0; i < 3; i++ {
		fileName := "test" + strconv.Itoa(i) + ".txt"
		testFilePath := filepath.Join(testDirPath, fileName)
		innerFilePath := filepath.Join(innerDir, fileName)

		f, err := os.Create(testFilePath)
		_, err = f.WriteString("Hello world")
		err = f.Close()

		f, err = os.Create(innerFilePath)
		_, err = f.WriteString("Hello world")
		err = f.Close()

		if err != nil {
			log.Fatal(err)
			return
		}
	}

	if err != nil {
		log.Fatal(err)
		return
	}

	// when

	RemoveFiles(testDirPath, false)

	// then

	filesCount := countFiles(testDirPath)
	if filesCount != 0 {
		t.Errorf("Not all files were deleted")
	}

	innerCount := countFiles(innerDir)
	if innerCount == 0 {
		t.Error("Deleted inner files in nonrecursive test")
	}
	// cleanup

	err = os.RemoveAll(testDirPath)
	if err != nil {
		log.Fatal(err)
		return
	}
}

func TestRecursiveRemoveAllFiles(t *testing.T) {
	// given
	cwd, err := os.Getwd()
	testDirPath := filepath.Join(cwd, "test-dir")
	innerDir := filepath.Join(testDirPath, "inner")

	err = os.Mkdir(testDirPath, 0755)
	err = os.Mkdir(innerDir, 0755)
	if err != nil {
		log.Fatal(err)
	}

	// create 3 text files
	for i := 0; i < 3; i++ {
		fileName := "test" + strconv.Itoa(i) + ".txt"
		testFilePath := filepath.Join(testDirPath, fileName)
		innerFilePath := filepath.Join(innerDir, fileName)

		f, err := os.Create(testFilePath)
		_, err = f.WriteString("Hello world")
		err = f.Close()

		f, err = os.Create(innerFilePath)
		_, err = f.WriteString("Hello world")
		err = f.Close()

		if err != nil {
			log.Fatal(err)
			return
		}
	}

	if err != nil {
		log.Fatal(err)
		return
	}

	// when

	RemoveFiles(testDirPath, true)

	// then

	filesCount := countFiles(testDirPath)
	if filesCount != 0 {
		t.Errorf("Not all files were deleted")
	}

	innerCount := countFiles(innerDir)
	if innerCount != 0 {
		t.Error("Did not delete files recursively")
	}
	// cleanup

	err = os.RemoveAll(testDirPath)
	if err != nil {
		log.Fatal(err)
		return
	}
}
